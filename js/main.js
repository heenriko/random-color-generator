/*
	AUTHOR: 	Henrik Oddløkken

	T A B L E   OF   C O N T ENTS 
			
		Declaration
			Array colours
			Class Colour
		
		Functions
			generateColours
			displayColours
			copyToClipboard
			createCanvas
		
*/

$(document).ready(function()
{
	new Clipboard('.btn');
	
/*---------------------------------------------------------------------------*/
/* 							D E C L R A T I O N
/*---------------------------------------------------------------------------*/
	
	// Declare array of colour objects:
	var colours = [];						
	
	/**
	 *	Class Colour
	 *	@param {Int} - Red value
	 *	@param {Int} - Green value
	 *	@param {Int} - Green value
	 *	Generate the hex value of this color.
	 *	@return void.
	 */
	var Colour = function(r,g,b) 			
	{
		// Set values:
		this.r = r.toString();
		this.g = g.toString();
		this.b = b.toString();
		
		// Converting to hex:
		// Thanks to FelipeC:
		// URL: http://stackoverflow.com/questions/5623838/rgb-to-hex-and-hex-to-rgb
		this.decToHex = function()
		{
			var rgb = b | (g << 8) | (r << 16);
			this.hex = '#' + (0x1000000 + rgb).toString(16).slice(1)
		}
				
	};
	
/*---------------------------------------------------------------------------*/
/* 							F U N C T I O N S
/*---------------------------------------------------------------------------*/
	/**
	 *	Fn Generate Colours
	 *	Generate a value from 0 - 255 for each RGB channel.
	 *	@return {Object} colour.
	 */
	function generateColours()
	{		
		var r = Math.floor((Math.random() * 255) + 1);
		var g = Math.floor((Math.random() * 255) + 1);
		var b = Math.floor((Math.random() * 255) + 1);
		
		return new Colour(r,g,b);
	}
	
	/**
	 *	Fn Display Colours
	 *	Generate HTML output for each colour in a list item.
	 *	@return void.
	 */
	function displayColours()
	{
		// Unordered list of colour samples:
		var colorList = $('.generatedColours'); 
		
		// Iterate all generated colours to output:
		for(var i = 0; i < colours.length; i++)
		{
			// RGB string values to set the bg-colour of the sample: 
			var rgb = "rgb(" + colours[i].r + "," + colours[i].g + "," + colours[i].b + ");"; 
			
			// HTML output:
			var li = '<li>' +
					 '<span class="hint--top" aria-label="Copy Hex">' +
					 '<div class="colourBox btn" data-clipboard-text="' + colours[i].hex + '" style = "background-color: ' + rgb + '" ></div></span>' +
					 '<div class="labels">' +
						'<div>R: ' + colours[i].r + '</div>' +
						'<div>G: ' + colours[i].g + '</div>' +
						'<div>B: ' + colours[i].b + '</div>' +
					 '</div></li>';
			
			// Append to the unordered list:	
			colorList.append(li);		
		}
	}
	
	function createCanvas()
	{
		var canvas = document.getElementById('canvas'),
		ctx = canvas.getContext('2d');
		
		var nrOfSamples = colours.length;
		var square = 92;
		var padding = 8;
		
		var ratio = (nrOfSamples * square);
		
		canvas.width = ratio;
		canvas.height = square;
		
		for(var i = 0; i < nrOfSamples; i++)
		{
			/* for(var j = 0; j < nrOfSamples / 2; j++)
			{ */
				ctx.fillStyle = colours[i].hex;
				ctx.fillRect(i * (square), 0, square, square);
				
			//}
		}
	}
	
	/**
	 * This is the function that will take care of image extracting and
	 * setting proper filename for the download.
	 * IMPORTANT: Call it from within a onclick event.
	*/
	function downloadCanvas(link, canvasId, filename) 
	{
		link.href = document.getElementById(canvasId).toDataURL();
		link.download = filename;
	}


/*---------------------------------------------------------------------------*/
/* 						W I N D O W   F U N C T I O N S
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/* 						U S E R   I N T E R A C T I O N
/*---------------------------------------------------------------------------*/
	/**
	 *	Event - Submit button to generate colours.
	 *	@return void.
	 */
	$('.generate').on('click',function()
	{
		// Find the corresponding input field of nr of samples:
		var inputField = $(this).parent().find('.nrSamples');
		
		// resets:
		colours = [];	
		$('.generatedColours').empty();
		
		// Number of samples to generate:
		var nrSamples = parseInt(inputField.val());
		
		if(nrSamples > 0)
		{
			if(nrSamples <= 12)
			{
				// Iterate the the samples:
				for(var i = 0; i < nrSamples; i++)
				{
					// Generate and add colour to the array:
					colours.push(generateColours());
					colours[i].decToHex();
				}
				
				displayColours(colours);
				
				// Show n hide HTML sections:
				$('#colourSection, .inputBottom').css('display','block');
				$('#inputSection').css('display','none');
				
				createCanvas();
			}
			else
				alert('Please enter maximum 12 colours');
		}
		else
			alert('Please enter atleast 1 color');
	});
	
	/**
	 *	Event - Change the view of the samples.
	 *	@return void.
	 */
	$('#colourSection').delegate('.sortGridReg','click',function()
	{
		$('.sort').removeClass('sortActive');
		$(this).addClass('sortActive');
		
		$('.generatedColours li .colourBox').css({'width':'64px', 'height':'64px'});
	});
	
	/**
	 *	Event - Change the view of the samples.
	 *	@return void.
	 */
	$('#colourSection').delegate('.sortGridLarge','click',function()
	{
		$('.sort').removeClass('sortActive');
		$(this).addClass('sortActive');
		
		$('.generatedColours li .colourBox').css({'width':'100px', 'height':'100px'});
	});
	
	/**
	 *	Event - Click to open the prompt window to copy the hex colour.
	 *	@return void.
	 */
	$('#colourSection').delegate('.colourBox','click',function()
	{
		var index = $('.colourBox').index(this);
		var hex = colours[index].hex;
		
		$('.feedback').html('Copied to clipBoard:<br> ' + hex);
	});
	
	/** 
	 * The event handler for the link's onclick event. We give THIS as a
	 * parameter (=the link element), ID of the canvas and a filename.
	*/
	document.getElementById('download').addEventListener('click', function() 
	{
		downloadCanvas(this, 'canvas', 'samples.png');
	}, false);

	
});// End doc ready.